from PIL import Image
import os
import shutil

img_dir_path = "/Users/ihyeon-u/Downloads/LeeProject/data/dog_nose_print_image_pool"
result_img_dir_path = "/Users/ihyeon-u/Downloads/LeeProject/data/dog_nose_print_image_pool_cropped"

width_height_ratio = (1, 1)  # 이미지를 crop 할 때의 너비와 높이의 비율을 정합니다.


def main_stream():
    if os.path.exists(result_img_dir_path):
        shutil.rmtree(result_img_dir_path)

    os.makedirs(result_img_dir_path)

    print("이미지 리스트 작성 중")
    possible_img_extension = ['.jpg', '.jpeg', '.JPG', '.bmp', '.png', '.PNG', 'BMP']
    img_path_list = []

    for (root, dirs, files) in os.walk(img_dir_path):
        if len(files) > 0:
            for file_name in files:
                if os.path.splitext(file_name)[1] in possible_img_extension:
                    img_path = root + '/' + file_name
                    img_path = img_path.replace('\\', '/')  # \는 \\로 나타내야함
                    img_path_list.append(img_path)

    print("파일 crop 중...")
    for img_path in img_path_list:
        img_name = img_path.split("/")[-1]
        img_obj = Image.open(img_path)
        img_size = img_obj.size
        crop_area = calc_crop_area(img_size)

        Image.open(img_path).crop(crop_area).save(
            result_img_dir_path + "/" + img_name)

    print("파일 crop 완료.")


def calc_crop_area(img_size):
    crop_center = (int(float(img_size[0]) / 2.0), int(float(img_size[1]) / 2.0))

    real_img_ratio = float(img_size[1]) / float(img_size[0])
    refine_img_ratio = float(width_height_ratio[1]) / float(width_height_ratio[0])

    if real_img_ratio < refine_img_ratio:  # 변경할 h/w 비율이 원본 비율보다 클때 = width 를 줄여서 height 비율을 키우고 싶을 때.
        img_refine_size = (
            int(float(img_size[1]) * (float(width_height_ratio[0]) / float(width_height_ratio[1]))),
            int(img_size[1]))  # crop 후의 이미지 사이즈
        bias = int(crop_center[0]) - int(float(img_refine_size[0]) / 2.0)  # refine size 에 따라 중심점으로부터 crop 할 공간의 좌표를 구함
        crop_area = (bias, 0, bias + img_refine_size[0], img_size[1])  # (x1, y1, x2, y2)
    else:  # 변경할 h/w 비율이 원본 비율보다 작을때 = height 를 줄여서 width 비율을 키우고 싶을 때.
        img_refine_size = (
            int(img_size[0]),
            int(float(img_size[0]) * (float(width_height_ratio[1]) / float(width_height_ratio[0]))))  # crop 후의 이미지 사이즈
        bias = int(crop_center[1]) - int(float(img_refine_size[1]) / 2.0)  # refine size 에 따라 중심점으로부터 crop 할 공간의 좌표를 구함
        crop_area = (0, bias, img_size[0], bias + img_refine_size[1])  # (x1, y1, x2, y2)

    return crop_area


if __name__ == '__main__':
    main_stream()
